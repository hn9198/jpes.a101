/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fa.training.assignment1;

import java.util.Scanner;

/**
 *
 * @author HienDepTrai
 */
public class CircleExercise {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Radius = ");
        double R = sc.nextDouble();
        double C = 2 * R * Math.PI;
        double S = R * R * Math.PI;
        System.out.println("Perimeter is = " + C);
        System.out.println("Area is là = " + S);
        
    }
}
