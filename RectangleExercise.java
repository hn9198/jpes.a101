/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fa.training.assignment1;

import java.util.Scanner;

/**
 *
 * @author HienDepTrai
 */
public class RectangleExercise {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Width = ");
        double w = sc.nextDouble();
        System.out.print("Height = ");
        double h = sc.nextDouble();
        double C = (w + h) * 2;
        double S = w * h;

        System.out.println("Area is " + w + " * " + h + " = " + S);
        System.out.print("Perimeter is " + 2 + " * (" + w + " + " + h + ")" + " = ");
        System.out.format("%.2f", C);
        System.out.println("\n");
    }
}
